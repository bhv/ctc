<?php
/**
 * Formatting API
 * 
 * Encode emoji.. 
 */

/**
 * Encoding emoji 
 * 
 * To check the charset and run
 * @uses wp_encode_emoji
 * 
 * @since 3.3.5
 * 
 * @param string $value		input value to convert emojis to html entity
 */
if ( ! function_exists('ht_ctc_wp_encode_emoji') ) {
function ht_ctc_wp_encode_emoji($value = '') {
	
	//todo
	if ('print' == $value) {
		global $wpdb;
		echo('<pre style="margin: 240px;">');
		print_r('DB_CHARSET: ');
		print_r(DB_CHARSET);
		print_r('<br>');
		print_r('$wpdb->collate: ');
		print_r($wpdb->collate);
		print_r('<br>');
		print_r('$wpdb->charset: ');
		print_r($wpdb->charset);
		print_r('<br>');

		print_r($value);
		print_r('<br>');
		echo('</pre>');
	}
		

	if ( defined('DB_CHARSET') && 'utf8' == DB_CHARSET ) {
		
		//todo
		if ('print' == $value) {
			echo('<pre style="margin: 240px;">');
			print_r('Its utf8 - run wp_encode_emoji');
			echo('</pre>');
		}

		if (function_exists('wp_encode_emoji')) {
			$value = wp_encode_emoji( $value );
		}

		return $value;
	}
}
}
